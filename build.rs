fn main() {
    git_version();
}

fn git_version() {
    // Emit the "cargo:" instructions including
    // "cargo:rustc-env=VERGEN_GIT_DESCRIBE=<DESCRIBE>".
    //
    // If the source directory does not contain a git repository,
    // e.g., because the code was extracted from a tarball, this
    // produces an `Error` result, which we ignore, and
    // `VERGEN_GIT_DESCRIBE` is not set.  That's okay, because we are
    // careful to only use `VERGEN_GIT_DESCRIBE` if it is actually
    // set.
    let _ = vergen::EmitBuilder::builder()
        // VERGEN_GIT_DESCRIBE
        .git_describe(/* dirty */ true, /* tags */ false, None)
        // Don't emit placeholder values for the git version if the
        // git repository is not present.
        .fail_on_error()
        .emit();
}
