//! Commit-tree traversal and verification.

use std::{
    path::{Path, PathBuf},
    collections::{BTreeMap, BTreeSet},
};

use anyhow::{anyhow, Result};
use git2::{
    Repository,
    Oid,
};

use sequoia_openpgp::{
    self as openpgp,
    Cert,
    crypto::hash::Digest,
    packet::{Signature, UserID},
    types::HashAlgorithm,
};

use crate::{
    Error,
    Policy,
    persistent_set,
};

/// Whether to trace execution by default (on stderr).
const TRACE: bool = false;

#[derive(Default, Debug)]
pub struct VerificationResult {
    pub signer_keys: BTreeSet<openpgp::Fingerprint>,
    pub primary_uids: BTreeSet<UserID>,
}

pub fn verify(git: &Repository,
              trust_root: Oid,
              shadow_policy: Option<Policy>,
              commit_range: (Oid, Oid),
              results: &mut VerificationResult,
              keep_going: bool,
              mut verify_cb: impl FnMut(&Oid, &crate::Result<Vec<crate::Result<(String, Signature, Cert)>>>) -> crate::Result<()>,
              cache: &mut VerificationCache)
              -> Result<()> {
    tracer!(TRACE, "verify");
    t!("verify(_, {}, {}..{})", trust_root, commit_range.0, commit_range.1);

    // Corner case of the zero-length path covered by a shadow policy.
    if commit_range.0 == commit_range.1 {
        if let Some(p) = &shadow_policy {
            let vresult = p.verify(git, &commit_range.0, p,
                                   &mut results.signer_keys,
                                   &mut results.primary_uids);
            verify_cb(&commit_range.0, &vresult)?;
            return vresult.map(|_| ()).map_err(Into::into);
        } else {
            return Err(Error::EmptyCommitRange.into());
        }
    }

    // Strategy: Find all paths to the root.  Record them in a
    // queue.  Compute and remember the length of each path, so that
    // we can verify short ones first.
    #[derive(Clone, Debug)]
    struct Item {
        oid: Oid,
        length: usize,
        next: Option<usize>,
        verified: Option<bool>,
    }

    let mut queue: Vec<Item> = Vec::new();
    queue.push(Item {
        oid: commit_range.1,
        length: 0,
        next: None,
        verified: None,
    });

    let mut paths: BTreeMap<usize, Vec<usize>> = BTreeMap::default();
    for scan in 0..usize::MAX {
        let item = if let Some(i) = queue.get(scan).cloned() {
            i
        } else {
            break;
        };
        t!("Visiting queue[{}] = {:?}", scan, item);

        let commit = git.find_commit(item.oid)?;
        if commit.parents().any(|p| p.id() == commit_range.0) {
            let length = item.length + 1;
            t!("Found path of length {} starting at {}", length, scan);
            paths.entry(length).or_default().push(scan);

            continue;
        }

        for parent in commit.parents() {
            queue.push(Item {
                oid: parent.id(),
                length: item.length + 1,
                next: Some(scan),
                verified: None,
            });
        }
    }

    if paths.is_empty() {
        return Err(Error::NoPathConnecting(commit_range.0, commit_range.1)
                   .into());
    }

    t!("Paths by length: {:?}", paths);
    let root_policy = match Policy::read_from_commit(git, &trust_root) {
        Ok(p) => p,
        Err(e) => if let Some(p) = &shadow_policy {
            p.clone()
        } else {
            return Err(e.into());
        }
    };
    let mut errors = Vec::new();
    let mut verified_path = None;
    'search: for (length, starts) in paths {
        t!("Considering all paths of length {}", length);
        for start in starts {
            t!("Considering path starting at {}", start);
            let mut p = root_policy.clone();
            let mut n = start;
            let mut path_valid = true;
            'path: loop {
                let item = &mut queue[n];
                t!("Visiting queue[{}] = {:?}", n, item);

                if let Some(result) = item.verified {
                    t!("Previously verified as {}", result);
                    break 'path;
                }

                let item_policy =
                    match Policy::read_from_commit(git, &item.oid) {
                        Ok(p) => p,
                        Err(e) => if let Some(p) = &shadow_policy {
                            p.clone()
                        } else {
                            return Err(e.into());
                        },
                    };

                let (vresult, cache_hit) = if cache.contains(item.oid)? {
                    (Ok(vec![]), true)
                } else {
                    (p.verify(git, &item.oid, &item_policy,
                              &mut results.signer_keys,
                              &mut results.primary_uids),
                     false)
                };
                verify_cb(&item.oid, &vresult)?;
                item.verified = match vresult {
                    Ok(results) => {
                        let mut good = false;

                        // Handle goodlisted commits.
                        if cache_hit {
                            // XXX: communicate this to the caller
                            // instead of eprintln.
                            eprintln!("{}: Cached positive verification",
                                      item.oid);
                            good = true;
                        } else if results.is_empty() {
                            // XXX: communicate this to the caller
                            // instead of eprintln.
                            eprintln!("{}: Explicitly goodlisted", item.oid);
                            good = true;
                        }

                        for r in results {
                            match r {
                                Ok(_) => good = true,
                                Err(e) => errors.push(
                                    anyhow::Error::from(e).context(
                                        format!("While verifying commit {}", item.oid))),
                            }
                        }

                        if ! cache_hit && good {
                            cache.insert(item.oid)?;
                        }

                        Some(good)
                    },
                    Err(e) => {
                        errors.push(anyhow::Error::from(e).context(
                            format!("While verifying commit {}", item.oid)));
                        Some(false)
                    },
                };
                if let Some(false) = &item.verified {
                    // XXX: Actually, we should continue the search
                    // and remember the bad commit.  Then, if we later
                    // find a recovery commit that adds the bad
                    // commits to the goodlist, we can recover!
                    path_valid = false;
                    if ! keep_going {
                        t!("Terminating path exploration.");
                        break 'path;
                    }
                }

                if let Some(next) = item.next {
                    p = item_policy;
                    n = next;
                } else {
                    if path_valid {
                        t!("Verified path starting at {}", start);
                        verified_path = Some(start);
                        if ! keep_going {
                            t!("Terminating search.");
                            break 'search;
                        }
                    }
                }
            }
        }
    }

    if verified_path.is_some() {
        Ok(())
    } else {
        if errors.is_empty() {
            Err(anyhow!("Could not verify commits {}..{}",
                        commit_range.0, commit_range.1))
        } else {
            let mut e = errors.swap_remove(0)
                .context(format!("Could not verify commits {}..{}",
                                 commit_range.0, commit_range.1));
            if ! errors.is_empty() {
                e = e.context(format!("{} commits failed to verify.  \
                                       The first error is shown:",
                                      errors.len() + 1));
            }
            Err(e)
        }
    }
}

pub struct VerificationCache {
    path: PathBuf,
    set: persistent_set::Set,
    digest: Box<dyn Digest>,
}

impl VerificationCache {
    const CONTEXT: &'static str = "SqGitVerify0";

    pub fn for_policy(p : &Policy) -> Result<Self> {
        Self::new(dirs::cache_dir().ok_or(anyhow::anyhow!("No cache dir"))?
                  .join("sq-git.verification.cache"), p)
    }

    pub fn new<P: AsRef<Path>>(path: P, for_policy: &Policy) -> Result<Self> {
        let digest = {
            let mut p = Vec::new();
            serde_json::to_writer(&mut p, for_policy)?;

            let mut d = HashAlgorithm::SHA512.context()?;
            d.update(Self::CONTEXT.as_bytes());
            d.update(&p);
            d
        };

        Ok(VerificationCache {
            path: path.as_ref().into(),
            set: persistent_set::Set::read(path, Self::CONTEXT)?,
            digest,
        })
    }

    fn key(&self, id: Oid) -> Result<persistent_set::Value> {
        let mut d = self.digest.clone();
        d.update(id.as_bytes());
        let mut key = [0; 32];
        d.digest(&mut key)?;
        Ok(key)
    }

    pub fn contains(&mut self, id: Oid) -> Result<bool> {
        Ok(self.set.contains(&self.key(id)?)?)
    }

    pub fn insert(&mut self, id: Oid) -> Result<()> {
        self.set.insert(self.key(id)?);
        Ok(())
    }

    pub fn persist(&mut self) -> Result<()> {
        self.set.write(&self.path)?;
        Ok(())
    }
}
