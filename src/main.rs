use std::{
    env,
    io,
    ops::Deref,
    path::PathBuf,
};
use anyhow::{anyhow, Context, Result};
use clap::Parser;
use once_cell::sync::OnceCell;

use sequoia_openpgp::{
    self as openpgp,
    Cert,
    KeyHandle,
};
use sequoia_cert_store::{
    CertStore,
    Store,
};
use sequoia_git::*;

#[macro_use]
mod macros;

mod output;
#[allow(dead_code)]
mod utils;

#[derive(Parser)]
#[command(
    author,
    version = format!("{}{}",
                      clap::crate_version!(),
                      if let Some(v) = option_env!("VERGEN_GIT_DESCRIBE") {
                          format!("-g{}", v)
                      } else {
                          "".into()
                      }),
    about,
    long_about = None,
)]
pub struct Cli {
    #[clap(
        long,
        help = "Disables the use of a certificate store",
        long_help = format!("\
Disables the use of a certificate store.  By default, sq-git uses the \
user's standard cert-d, which is located in \"{}\".",
                            cert_store_base().display()),
    )]
    pub no_cert_store: bool,

    #[clap(
        long,
        value_name = "PATH",
        env = "SQ_CERT_STORE",
        conflicts_with_all = &[ "no_cert_store" ],
        help = "Specifies the location of the certificate store",
        long_help = format!("\
Specifies the location of the certificate store.  By default, sq-git \
uses the the user's standard cert-d, which is located in \"{}\".",
                            cert_store_base().display()),
    )]
    pub cert_store: Option<PathBuf>,

    /// Use POLICY instead of the repository's policy.
    ///
    /// The default policy is stored in the "openpgp-policy.toml"
    /// file in the root of the repository.
    #[arg(global = true, long, value_name = "POLICY")]
    policy_file: Option<PathBuf>,

    #[arg(
        global = true,
        long = "output-format",
        value_name = "FORMAT",
        value_parser = ["human-readable", "json"],
        default_value = "human-readable",
        help = "Produces output in FORMAT, if possible",
    )]
    pub output_format: String,

    #[command(subcommand)]
    pub subcommand: Subcommand,
}

/// Describe, update, and change the OpenPGP policy.
#[derive(clap::Subcommand)]
pub enum PolicySubcommand {
    /// Describes the policy.
    ///
    /// This reads in the policy default and dumps it in a more
    /// descriptive format on stdout.
    Describe {
    },

    /// Changes the authorizations.
    Authorize {
        name: String,

        /// The fingerprint or Key ID of the certificate to XXX authenticate
        ///
        /// This is read from the user's default certificate
        /// directory.  On Unix-like systems, this is usually located
        /// in "$HOME/.local/share/pgp.cert.d".
        #[command(flatten)]
        cert: CertArg,

        /// Grant the certificate the sign-commit capability.
        ///
        /// This capability allows the certificate to sign commits.
        /// That is, when authenticating a version of the repository,
        /// a commit is considered authenticated if it is signed by a
        /// certificate with this capability.
        #[arg(long, overrides_with = "no_sign_commit")]
        sign_commit: bool,

        /// Rescind the sign-commit capability from a certificate.
        ///
        /// Removes the sign-commit capability for the certificate.
        /// Note: this operation is not retroactive; commits signed
        /// with the certificate, and made prior to the policy change
        /// are still considered authenticated.
        #[clap(long)]
        no_sign_commit: bool,

        /// Grant the certificate the sign-tag capability.
        ///
        /// This capability allows the certificate to sign tags.  That
        /// is, when authenticating a tag, a tag is considered
        /// authenticated if it is signed by a certificate with this
        /// capability.
        #[arg(long, overrides_with = "no_sign_tag")]
        sign_tag: bool,
        /// Rescind the sign-tag capability from a certificate.
        ///
        /// Removes the sign-tag capability for the certificate.
        /// Note: this operation is not retroactive; tags signed with
        /// the certificate, and made prior to the policy change are
        /// still considered authenticated.
        #[clap(long)]
        no_sign_tag: bool,

        /// Grant the certificate the sign-archive capability.
        ///
        /// This capability allows the certificate to sign tarballs or
        /// other archives.  That is, when authenticating an archive,
        /// an archive is considered authenticated if it is signed by
        /// a certificate with this capability.
        #[arg(long, overrides_with = "no_sign_archive")]
        sign_archive: bool,
        /// Rescind the sign-archive capability from a certificate.
        ///
        /// Removes the sign-archive capability for the certificate.
        /// Note: this operation is not retroactive; archives signed
        /// with the certificate prior to the policy change are still
        /// considered authenticated.
        #[clap(long)]
        no_sign_archive: bool,

        /// Grant the certificate the add-user capability.
        ///
        /// This capability allows the certificate add users to the
        /// policy file, and to grant them capabilities.  A
        /// certificate that has this capability is only allowed to
        /// grant capabilities that it has.  That is, if Alice has the
        /// "sign-commit" and "add-user" capability, she can grant Bob
        /// either of those capabilities, but she is can't grant him
        /// the "sign-tag" capability, because she does not have that
        /// capability.
        #[arg(long, overrides_with = "no_add_user")]
        add_user: bool,
        /// Rescind the add-user capability from a certificate.
        ///
        /// Removes the add-user capability for the certificate.
        /// Note: this operation is not retroactive; operations that
        /// rely on this grant prior to the policy change are still
        /// considered authenticated.
        ///
        /// Rescinding the add-user capability from a certificate does
        /// not rescind any grants that that certificate made.  That
        /// is, if Alice grants Bob the can-sign and add-user
        /// capability, Bob grants Carol the can-sign capability, and
        /// then Alice rescinds Bob's can-sign and add-user
        /// capabilities, Carol still has the can-sign capability.  In
        /// this way, a grant is a copy of a capability.
        #[clap(long)]
        no_add_user: bool,

        /// Grants the certificate the retire-user capability.
        ///
        /// This capability allows the certificate to rescind
        /// arbitrary capabilities.  That is, if Alice has the
        /// retire-user capability, she can rescind Bob's can-sign
        /// capability even if she didn't grant him that capability.
        #[arg(long, overrides_with = "no_retire_user")]
        retire_user: bool,
        /// Rescind the retire-user capability from a certificate.
        ///
        /// Removes the retire-user capability from a certificate.
        /// The specified certificate cannot no longer rescind
        /// capabilities even those that they granted.
        #[clap(long)]
        no_retire_user: bool,

        /// Grants the certificate the audit capability.
        ///
        /// This capability allows the certificate to audit commits.
        /// If Alice has the audit capability, Bob has the can-sign
        /// capability, and then Bob revokes his key, because it was
        /// compromised, then all commits that Bob signed are
        /// considered invalid.  Alice can recover from this situation
        /// by auditing Bob's commit.  After auditing each commit, she
        /// marks it as good.
        #[arg(long, overrides_with = "no_audit")]
        audit: bool,
        /// Rescind the audit capability from a certificate.
        ///
        /// Removes the audit capability from a certificate.  The
        /// specified certificate cannot no longer mark arbitrary
        /// commits as good.
        #[clap(long)]
        no_audit: bool,
    },

    /// Updates the OpenPGP certificates in the policy.
    ///
    /// "sq-git" looks for updates to the certificates listed in the
    /// policy file in the user's certificate store.
    ///
    /// # Examples
    ///
    /// # Download updates to the specified certificate from the
    /// # configured keyserver to the local certificate store.
    /// $ sq keyserver get F7173B3C7C685CD9ECC4191B74E445BA0E15C957
    ///
    /// # Look for updates in the local certificate store.
    /// $ sq-git policy sync
    Sync {
    },

    /// Adds the given commit to the commit goodlist.
    ///
    /// This requires the audit capability.
    Goodlist {
        commit: String,
    },
}

#[derive(clap::Subcommand)]
pub enum Subcommand {
    Policy {
        #[command(subcommand)]
        command: PolicySubcommand,
    },

    /// Lists commits, verifying that the commits up to the given
    /// trust root adhere to the policy.
    Log {
        /// Specifies the trust root.
        ///
        /// A version is considered authenticated if the trust root is
        /// an ancestor of the version being authenticated, and each
        /// commit can be authenticated by at least one of its parent
        /// commits' policy.
        ///
        /// If no policy is specified, then the local branch named
        /// `openpgp-trust-root` is used as the trust root.
        #[arg(long, value_name = "COMMIT")]
        trust_root: Option<String>,

        /// Continues to check commits even when it is clear that the
        /// version cannot be authenticated.
        ///
        /// Cause "sq-git" to continue to check commits rather than
        /// stopping as soon as it is clear that the version can't be
        /// authenticated.
        #[arg(long)]
        keep_going: bool,

        /// After authenticating the current version, prunes the
        /// certificates.
        ///
        /// After authenticating the current version, this prunes
        /// unused components of the certificates.  In particular,
        /// subkeys that were not used to verify a signature, and User
        /// IDs that were never considered primary are removed.
        ///
        /// This does not remove unused certificates from the policy
        /// file; this just minimizes them.
        #[arg(long)]
        prune_certs: bool,

        /// The commits to check.
        ///
        /// If not specified, commits between the trust root and HEAD
        /// are checked.
        commit_range: Option<String>,
    },

    /// Verifies signatures on archives like release tarballs.
    Verify {
        /// Read the policy from this commit.  Falls back to using the
        /// `openpgp-trust-root` branch.  Can be overridden using
        /// `--policy-file`.
        #[arg(long, value_name = "COMMIT")]
        trust_root: Option<String>,

        /// The signature to verify.
        #[arg(long, value_name = "FILENAME")]
        signature: PathBuf,

        /// The archive that the signature protects.
        #[arg(long, value_name = "FILENAME")]
        archive: PathBuf,
    },

    /// git update hook that enforces an OpenPGP policy.
    ///
    /// Insert the following line into `hooks/update` on the shared
    /// git server to make it enforce the policy embedded in the
    /// repository starting at the given trust root `COMMIT`.
    ///
    ///     sq-git update-hook --trust-root=<COMMIT> "$@"
    ///
    /// When a branch is pushed that is not previously known to the
    /// server, sq-git checks that all commits starting from the trust
    /// root to the pushed commit adhere to the policy.
    ///
    /// When a branch is pushed that is previously known to the
    /// server, i.e. the branch is updated, sq-git checks that
    /// all new commits starting from the commit previously known to
    /// the server to the pushed commit adhere to the policy.  If
    /// there is no path from the previously known commit to the new
    /// one, the branch has been rebased.  Then, we fall back to
    /// searching a path from the trust root.
    UpdateHook {
        /// The commit to use as a trust root.
        #[arg(long, value_name = "COMMIT", required = true)]
        trust_root: String,

        /// The name of the ref being updated (supplied as first
        /// argument to the update hook, see githooks(5)).
        ref_name: String,

        /// The old object name stored in the ref (supplied as second
        /// argument to the update hook, see githooks(5)).
        old_object: String,

        /// The new object name stored in the ref (supplied as third
        /// argument to the update hook, see githooks(5)).
        new_object: String,
    }
}

#[derive(clap::Args, Debug)]
pub struct CertArg {
    /// The fingerprint or Key ID of the certificate to authenticate.
    #[arg(value_name="FINGERPRINT|KEYID")]
    cert: KeyHandle
}

impl Deref for CertArg {
    type Target = KeyHandle;

    fn deref(&self) -> &Self::Target {
        &self.cert
    }
}

/// Returns the cert store's base directory.
fn cert_store_base() -> PathBuf {
    // XXX: openpgp-cert-d doesn't yet export this:
    // https://gitlab.com/sequoia-pgp/pgp-cert-d/-/issues/34
    // Remove this when it does.
    dirs::data_dir()
        .expect("Unsupported platform")
        .join("pgp.cert.d")
}

fn main() -> anyhow::Result<()> {
    let policy = openpgp::policy::StandardPolicy::new(); // XXX
    let cli = Cli::parse();
    let output_format: output::Format = cli.output_format.parse()?;

    let _cert_store: OnceCell<CertStore> = OnceCell::new();

    // Returns the cert store.
    //
    // If it is not yet open, opens it.
    //
    // If it does not exist, issues a warning and returns an empty
    // cert store.
    let cert_store = || -> Result<&CertStore> {
        _cert_store.get_or_try_init(|| {
            if cli.no_cert_store {
                Ok(CertStore::empty())
            } else {
                let path = cli.cert_store.clone()
                    .unwrap_or_else(|| cert_store_base().into());

                match path.metadata() {
                    Ok(metadata) => {
                        if metadata.is_dir() {
                            CertStore::open(&path)
                        } else {
                            Err(anyhow::anyhow!(
                                "Not a certificate directory"))
                        }
                    }
                    Err(err) => {
                        if err.kind() == std::io::ErrorKind::NotFound {
                            eprintln!("Warning: no certificate \
                                       store found at {}",
                                      path.display());
                            Ok(CertStore::empty())
                        } else {
                            Err(err.into())
                        }
                    }
                }
                .with_context(|| {
                    format!("While opening the certificate store at {:?}",
                            &path)
                })
            }
        })
    };

    let read_policy = || {
        if let Some(path) = &cli.policy_file {
            Policy::read(path)
                .with_context(|| {
                    format!("Reading specified policy file: {}",
                            path.display())
                })
        } else {
            Policy::read_from_working_dir()
                .with_context(|| {
                    format!("Reading default policy file")
                })
        }
    };

    let write_policy = |p: &Policy| {
        if let Some(path) = &cli.policy_file {
            p.write(path)
                .with_context(|| {
                    format!("Updating the specified policy file: {}",
                            path.display())
                })
        } else {
            p.write_to_working_dir()
                .with_context(|| {
                    format!("Updating default policy file")
                })
        }
    };

    let git_repo = || {
        let cwd = env::current_dir()
            .context("Getting current working directory")?;
        git2::Repository::discover(&cwd)
            .with_context(|| {
                format!("Looking for git repository under {}",
                        cwd.display())
            })
    };

    let lookup_trust_root = |git: &git2::Repository,
                             trust_root: Option<&str>|
    {
        match trust_root {
            Some(t) => {
                git2::Oid::from_str(&t)
                    .with_context(|| {
                        format!("Reading commit {}", t)
                    })
            }
            None => {
                let branch = git.find_branch(
                    "openpgp-trust-root",
                    git2::BranchType::Local)
                    .context("Local Branch openpgp-trust-root \
                              not found.  Create it or specify \
                              a trust root manually \
                              using \"--trust-root HASH\".")?;
                branch.get().target()
                    .context("Resolving local branch \
                              openpgp-trust-root")
            }
        }
    };

    match cli.subcommand {
      Subcommand::Policy { command } => match command {
        PolicySubcommand::Describe {
        } => {
            let p = read_policy()?;

            match output_format {
                output::Format::HumanReadable => {
                    output::describe_policy(&p)?;
                },
                output::Format::Json =>
                    serde_json::to_writer_pretty(io::stdout(), &p)?,
            }
        },

        PolicySubcommand::Authorize {
            name, cert,
            sign_commit, no_sign_commit,
            sign_tag, no_sign_tag,
            sign_archive, no_sign_archive,
            add_user, no_add_user,
            retire_user, no_retire_user,
            audit, no_audit,
        } => {
            let cert_store = cert_store()?;
            let certs = cert_store.lookup_by_key(&cert)?;

            let cert = match certs.len() {
                0 => return Err(anyhow!("Key {} not found", cert.cert)),
                1 => certs[0].to_cert()?.clone(),
                n => return Err(anyhow!("Key {} is part of {} certs, use cert \
                                         fingerprint to resolve",
                                        cert.cert, n)),
            };
            let fp = cert.fingerprint();

            let old_policy = read_policy()?;
            let mut p = old_policy.clone();

            let a = p.authorization.entry(name).or_default();
            let mut merged = false;
            let mut updated = Vec::new();
            for c in a.certs()? {
                let mut c = Cert::try_from(c?)?;
                if c.fingerprint() == fp {
                    c = c.merge_public(cert.clone())?;
                    merged = true;
                }
                updated.push(c);
            }
            if ! merged {
                updated.push(cert);
            }
            a.set_certs(updated)?;

            a.sign_commit =
                (a.sign_commit | sign_commit) & !no_sign_commit;
            a.sign_tag =
                (a.sign_tag | sign_tag) & !no_sign_tag;
            a.sign_archive =
                (a.sign_archive | sign_archive) & !no_sign_archive;

            a.add_user =
                (a.add_user | add_user) & !no_add_user;
            a.retire_user =
                (a.retire_user | retire_user) & !no_retire_user;

            a.audit =
                (a.audit | audit) & !no_audit;

            let diff = old_policy.diff(&p)?;
            match output_format {
                output::Format::HumanReadable => {
                    output::describe_diff(&diff)?;
                },
                output::Format::Json =>
                    serde_json::to_writer_pretty(io::stdout(), &diff)?,
            }

            write_policy(&p)?;
        },

        PolicySubcommand::Sync {
        } => {
            let old_policy = read_policy()?;
            let mut p = old_policy.clone();

            for a in p.authorization.values_mut() {
                let mut changed = false;
                let mut updated = Vec::new();
                for cert in a.certs()? {
                    let mut cert = Cert::try_from(cert?)?;
                    let fp = cert.fingerprint();
                    if let Ok(c) = cert_store()?.lookup_by_cert_fpr(&fp)
                        .and_then(|lc| lc.to_cert().cloned())
                    {
                        cert = cert.merge_public(c)?;
                        changed = true; // XXX: Overestimation.
                    }
                    updated.push(cert);
                }
                if changed {
                    a.set_certs(updated)?;
                }
            }

            let diff = old_policy.diff(&p)?;
            match output_format {
                output::Format::HumanReadable => {
                    output::describe_diff(&diff)?;
                },
                output::Format::Json =>
                    serde_json::to_writer_pretty(io::stdout(), &diff)?,
            }

            write_policy(&p)?;
        },

        PolicySubcommand::Goodlist {
            commit,
        } => {
            let old_policy = read_policy()?;
            let mut p = old_policy.clone();

            p.commit_goodlist.insert(commit);

            let diff = old_policy.diff(&p)?;
            match output_format {
                output::Format::HumanReadable => {
                    output::describe_diff(&diff)?;
                },
                output::Format::Json =>
                    serde_json::to_writer_pretty(io::stdout(), &diff)?,
            }

            write_policy(&p)?;
        },
      },

        Subcommand::Log {
            trust_root,
            keep_going,
            prune_certs,
            commit_range,
        } => {
            if prune_certs && commit_range.is_some() && cli.policy_file.is_none() {
                return Err(anyhow!("--prune-certs can only modify \
                                    HEAD or a shadow policy"));
            }

            let git = git_repo()?;
            let trust_root = lookup_trust_root(&git, trust_root.as_deref())?;

            let shadow_p = if let Some(s) = &cli.policy_file {
                Some(Policy::read(s)?)
            } else {
                None
            };

            let head = git.head()?.target().unwrap();
            let (start, target) = if let Some(commit_range) = commit_range {
                let mut s = commit_range.splitn(2, "..");
                let first = s.next().expect("always one component");
                if let Some(second) = s.next() {
                    if second.is_empty() {
                        (git2::Oid::from_str(first)?, head)
                    } else {
                        (git2::Oid::from_str(first)?,
                         git2::Oid::from_str(second)?)
                    }
                } else {
                    (trust_root, git2::Oid::from_str(first)?)
                }
            } else {
                (trust_root, head)
            };

            let mut cache = if let Some(p) = shadow_p.as_ref() {
                VerificationCache::for_policy(p)?
            } else {
                let p = Policy::read_from_commit(&git, &trust_root)
                    .with_context(|| {
                        format!("Reading policy from \
                                 trust root's commit")
                    })?;
                VerificationCache::for_policy(&p)?
            };

            let mut vresults = VerificationResult::default();
            let result = match output_format {
                output::Format::HumanReadable => {
                    verify(&git, trust_root, shadow_p,
                           (start, target),
                           &mut vresults,
                           keep_going,
                           |oid, result| {
                               output::Commit::new(
                                   &git, oid, &cli.policy_file, result)?
                                   .describe(&mut io::stdout())?;
                               Ok(())
                           },
                           &mut cache,
                    )
                },
                output::Format::Json => {
                    use serde::ser::{Serializer, SerializeSeq};
                    let mut serializer = serde_json::ser::Serializer::pretty(
                        std::io::stdout());
                    let mut seq = serializer.serialize_seq(None)?;
                    let r =
                        verify(&git, trust_root, shadow_p,
                               (start, target),
                               &mut vresults,
                               keep_going,
                               |oid, result| {
                                   seq.serialize_element(
                                       &output::Commit::new(
                                           &git, oid, &cli.policy_file, result)?
                                   ).map_err(anyhow::Error::from)?;
                                   Ok(())
                               },
                               &mut cache,
                        );
                    seq.end()?;
                    r
                },
            };

            if prune_certs {
                let mut p = read_policy()?;

                for a in p.authorization.values_mut() {
                    let certs =
                        a.certs()?
                        .map(|r| r.and_then(Cert::try_from))
                        .collect::<Result<Vec<_>>>()?;

                    a.set_certs_filter(
                        certs,
                        // Keep all subkeys that made a signature, and
                        // those that are alive now.
                        |sk| {
                            let fp = sk.fingerprint();
                            vresults.signer_keys.contains(&fp)
                                || {
                                    // Slightly awkward, because we
                                    // cannot use sk.with_policy.
                                    let c = sk.cert();

                                    c.with_policy(&policy, None)
                                        .map(|vka| vka.keys().key_handle(fp)
                                             .next().is_some())
                                        .unwrap_or(false)
                                }
                        },
                        // Keep all user IDs that were primary user
                        // IDs when a signature was made, and the ones
                        // that are the primary userid now.
                        |uid| vresults.primary_uids.contains(&uid)
                            || {
                                // Slightly awkward, because we
                                // cannot use sk.with_policy.
                                let c = uid.cert();

                                c.with_policy(&policy, None)
                                    .and_then(|vka| vka.primary_userid())
                                    .map(|u| u.userid() == uid.userid())
                                    .unwrap_or(false)
                            }
                    )?;
                }

                write_policy(&p)?;
            }

            let _ = cache.persist();
            result?;
        },

        Subcommand::Verify {
            trust_root,
            signature,
            archive,
        } => {
            let git = git_repo()?;
            let policy = if let Some(s) = &cli.policy_file {
                Policy::read(s)
                    .with_context(|| {
                        format!("Reading specified policy file: {}",
                                s.display())
                    })?
            } else {
                let trust_root = lookup_trust_root(
                    &git, trust_root.as_deref())?;

                Policy::read_from_commit(&git, &trust_root)
                    .with_context(|| {
                        format!("Reading policy from commit {}",
                                trust_root)
                    })?
            };

            // XXX: In the future, mmap the data.
            let signature = std::fs::read(&signature)
                .with_context(|| {
                    format!("Reading signature data from {}",
                            signature.display())
                })?;
            let archive = std::fs::read(&archive)
                .with_context(|| {
                    format!("Reading archive data from {}",
                            archive.display())
                })?;

            let r = policy.verify_archive(signature, archive);
            let o = output::Archive::new(r)?;
            match output_format {
                output::Format::HumanReadable =>
                    o.describe(&mut io::stdout())?,
                output::Format::Json =>
                    serde_json::to_writer_pretty(io::stdout(), &o)?,
            }
        },

        Subcommand::UpdateHook {
            trust_root,
            ref_name: _,
            old_object,
            new_object,
        } => {
            let git = git_repo()?;
            let trust_root = git2::Oid::from_str(&trust_root)
                .with_context(|| {
                    format!("Looking up specified trust root ({})",
                            trust_root)
                })?;
            let new_object = git2::Oid::from_str(&new_object)
                .with_context(|| {
                    format!("Looking up new object ({})",
                            new_object)
                })?;
            let old_object = git2::Oid::from_str(&old_object)
                .with_context(|| {
                    format!("Looking up old object ({})",
                            old_object)
                })?;

            // Fall back to the trust root if this is a new branch.
            let start = if old_object.is_zero() {
                trust_root
            } else {
                old_object
            };

            let policy = Policy::read_from_commit(&git, &trust_root)
                .with_context(|| {
                    format!("Reading policy file from trust root ({})",
                            trust_root)
                })?;
            let mut cache = VerificationCache::for_policy(&policy)?;
            let mut vresults = VerificationResult::default();
            let mut result = verify(&git, trust_root,
                   None,
                   (start, new_object),
                   &mut vresults,
                   false,
                   |oid, result| {
                       output::Commit::new(
                           &git, oid, &cli.policy_file, result)?
                           .describe(&mut io::stdout())?;
                       Ok(())
                   },
                   &mut cache,
            );

            // See if the update is a rebase.
            if ! old_object.is_zero()
                && result.as_ref().err()
                .and_then(|e| e.downcast_ref::<sequoia_git::Error>())
                .map(|e| matches!(e, Error::NoPathConnecting(_, _)))
                .unwrap_or(false)
            {
                // Treat like a new branch and search from the trust
                // root.
                result = verify(&git, trust_root,
                   None,
                   (trust_root, new_object),
                   &mut vresults,
                   false,
                   |oid, result| {
                       output::Commit::new(
                           &git, oid, &cli.policy_file, result)?
                           .describe(&mut io::stdout())?;
                       Ok(())
                   },
                   &mut cache,
                );
            }

            let _ = cache.persist();
            result?;
        },
    }
    Ok(())
}
