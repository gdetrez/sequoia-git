use std::io;
use git2::{Oid, Repository};
use serde::Serialize;
use openpgp::{
    Cert,
    packet::Signature,
};
use super::*;

/// What output format to prefer, when there's an option?
#[derive(Clone)]
pub enum Format {
    /// Output that is meant to be read by humans, instead of programs.
    ///
    /// This type of output has no version, and is not meant to be
    /// parsed by programs.
    HumanReadable,

    /// Output as JSON.
    Json,
}

impl std::str::FromStr for Format {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "human-readable" => Ok(Self::HumanReadable),
            "json" => Ok(Self::Json),
            _ => Err(anyhow!("unknown output format {:?}", s)),
        }
    }
}

/// Emits a human-readable description of the policy to stdout.
pub fn describe_policy(p: &Policy) -> Result<()> {
    println!("# OpenPGP policy file for git, version {}",
             p.version);
    println!();

    println!("## Commit Goodlist");
    println!();

    for commit in &p.commit_goodlist {
        println!("  - {}", commit);
    }
    println!();

    println!("## Authorizations");
    println!();

    for (i, (name, auth)) in p.authorization.iter().enumerate()
    {
        println!("{}. {}", i, name);
        let ident = vec![' '; i.to_string().len() + 2]
            .into_iter().collect::<String>();

        if auth.sign_commit {
            println!("{}- may sign commits", ident);
        }
        if auth.sign_tag {
            println!("{}- may sign tags", ident);
        }
        if auth.sign_archive {
            println!("{}- may sign archives", ident);
        }
        if auth.add_user {
            println!("{}- may add users", ident);
        }
        if auth.retire_user {
            println!("{}- may retire users", ident);
        }
        if auth.audit {
            println!("{}- may goodlist commits", ident);
        }
        for cert in auth.certs()? {
            println!("{}- has OpenPGP cert: {}", ident,
                     cert?.fingerprint());
        }
    }
    Ok(())
}

/// Emits a human-readable description of a difference between to
/// policies to stdout.
pub fn describe_diff(p: &Diff) -> Result<()> {
    for change in &p.changes {
        use Change::*;
        match change {
            VersionChange { from, to } =>
                println!("  - Version changed from {} to {}.", from, to),

            GoodlistCommit(oid) =>
                println!("  - Commit {} was added to the goodlist.", oid),
            UngoodlistCommit(oid) =>
                println!("  - Commit {} was removed from the goodlist.", oid),

            AddUser(name) =>
                println!("  - User {:?} was added.", name),

            RetireUser(name) =>
                println!("  - User {:?} was retired.", name),

            AddRight(name, right) =>
                println!("  - User {:?} was granted the right {}.", name, right),
            RemoveRight(name, right) =>
                println!("  - User {:?} was revoked the right {}.", name, right),

            _ => todo!("the cert stuff"),
        }
    }

    Ok(())
}

#[derive(Serialize)]
pub struct Commit<'a> {
    #[serde(serialize_with = "crate::utils::serialize_oid")]
    id: &'a Oid,
    results: Vec<std::result::Result<String, (String, Option<String>)>>,
}

impl<'a> Commit<'a> {
    pub fn new(_git: &Repository,
               id: &'a Oid,
               shadow_policy: &Option<PathBuf>,
               result: &'a sequoia_git::Result<Vec<sequoia_git::Result<(String, Signature, Cert)>>>)
               -> Result<Self>
    {
        let hint = |e: &Error| -> Option<String> {
            match (shadow_policy, e) {
                (None, _) => None,
                (Some(p), Error::MissingSignature(commit)) =>
                    Some(format!("to remedy, do\n\n\
                                  git show {1} \n\
                                  \n  and verify that the commit is good.  \
                                  If satisfied, do\n\n\
                                  sq-git goodlist --policy-file {0} {1}",
                                 p.display(), commit)),
                (Some(p), Error::MissingKey(handle)) =>
                    Some(format!("to remedy, do\n\n\
                                  sq keyserver get {1} \n\
                                  \n  and verify that the cert belongs to the \
                                  committer.  If satisfied, do\n\n\
                                  sq-git authorize --policy-file {} \
                                  <ROLE-NAME> {} --sign-commit",
                                 p.display(), handle)),
                _ => None,
            }
        };

        let mut r = Vec::new();
        match result {
            Ok(results) => {
                for e in results.iter()
                    .filter_map(|r| r.as_ref().err())
                {
                    r.push(Err((e.to_string(), hint(e))));
                }
                for (name, _s, c) in results.iter()
                    .filter_map(|r| r.as_ref().ok())
                {
                    r.push(Ok(format!("{} [{}]", name, c.keyid())));
                }
            },
            Err(e) => {
                r.push(Err((e.to_string(), hint(e))));
            },
        }

        Ok(Commit {
            id,
            results: r,
        })
    }

    pub fn describe(&self, sink: &mut dyn io::Write) -> Result<()> {
        for r in &self.results {
            match r {
                Err((e, hint)) => {
                    writeln!(sink, "{}: {}", self.id, e)?;
                    if let Some(h) = hint {
                        writeln!(sink, "\n  Hint: {}", h)?;
                    }
                },
                Ok(fp) => {
                    writeln!(sink, "{}: {}", self.id, fp)?;
                },
            }
        }

        Ok(())
    }
}

#[derive(Serialize)]
pub struct Archive {
    results: Vec<std::result::Result<String, String>>,
}

impl Archive {
    pub fn new(result: sequoia_git::Result<Vec<sequoia_git::Result<(String, Signature, Cert)>>>)
               -> Result<Self>
    {
        let mut r = Vec::new();
        match result {
            Ok(results) => {
                for e in results.iter()
                    .filter_map(|r| r.as_ref().err())
                {
                    r.push(Err(e.to_string()));
                }
                for (name, _s, c) in results.iter()
                    .filter_map(|r| r.as_ref().ok())
                {
                    r.push(Ok(format!("{} [{}]", name, c.keyid())));
                }
            },
            Err(e) => {
                r.push(Err(e.to_string()));
            },
        }

        Ok(Self {
            results: r,
        })
    }

    pub fn describe(&self, sink: &mut dyn io::Write) -> Result<()> {
        for r in &self.results {
            match r {
                Err(e) => {
                    writeln!(sink, "{}", e)?;
                },
                Ok(fp) => {
                    writeln!(sink, "{}", fp)?;
                },
            }
        }

        Ok(())
    }
}
