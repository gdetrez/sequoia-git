use git2::Oid;

use sequoia_openpgp::{
    self as openpgp,
};

#[macro_use]
mod macros;

mod policy;
pub use policy::*;

mod verify;
pub use verify::*;

pub mod persistent_set;
pub(crate) mod utils;

/// Errors for this crate.
#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Invalid operation: {0}")]
    InvalidOperation(String),
    #[error("Storage error: {0}")]
    StorageError(String),
    #[error("Commit {0} is not signed")]
    MissingSignature(Oid),
    #[error("{0} is not signed")]
    MissingDataSignature(String),
    #[error("Commit {0} has no policy")]
    MissingPolicy(Oid),
    #[error("The given range contains no commits")]
    EmptyCommitRange,
    #[error("There is no path from {0} to {1}")]
    NoPathConnecting(Oid, Oid),
    #[error("Key `{0}` missing")]
    MissingKey(openpgp::KeyHandle),
    #[error("Key `{0}` is bad: {1}")]
    BadKey(openpgp::KeyHandle, String),
    #[error("Bad signature: {0}")]
    BadSignature(String),
    #[error("Unauthorized: {0}")]
    Unauthorized(String),
    #[error("Io error")]
    Io(#[from] std::io::Error),
    #[error("Libgit2 error")]
    Git2(#[from] git2::Error),
    #[error("Other error: {0}")]
    Other(#[from] anyhow::Error),
}

/// Crate result specialization.
pub type Result<T> = ::std::result::Result<T, Error>;
