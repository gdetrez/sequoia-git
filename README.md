`sequoia-git` is tool that can be used to improve a project's supply
chain security.

To use `sequoia-git`, you add a policy file to the root of a `git`
repository, and authorize OpenPGP certificates to make different types
of changes.

A commit is considered authorized if the commit is signed, and that at
least one immediate parent commit's policy allows that certificate to
make that type of change.

A user authenticates a version by specifying a trust root (a commit),
which they've presumably audited.  `sq-git` starts with the version's
commit, authenticates the commit using each of its parent commits, and
then repeats the process for each parent commit.  If the trust root is
reached, then the version is considered valid.  For more details,
please refer to the specification.

See [the specification] for an in-depth discussion of semantics and
implementation.

  [the specification]: https://sequoia-pgp.gitlab.io/sequoia-git/

## Deploying `sq-git`

To start using `sequoia-git` in a repository, you first add one or
more certificates to the policy, and grant them particular rights.
This can be done using the `sq-git` tool as follows:

```shell
$ sq-git policy authorize --sign-commit --sign-tag --sign-archive --add-user --retire-user --audit 'Neal H. Walfield <neal@pep.foundation>' F7173B3C7C685CD9ECC4191B74E445BA0E15C957
  - User "Neal H. Walfield <neal@pep.foundation>" was granted the right sign-tag.
  - User "Neal H. Walfield <neal@pep.foundation>" was granted the right sign-archive.
  - User "Neal H. Walfield <neal@pep.foundation>" was granted the right add-user.
  - User "Neal H. Walfield <neal@pep.foundation>" was granted the right retire-user.
  - User "Neal H. Walfield <neal@pep.foundation>" was granted the right audit.
$ sq-git policy authorize --sign-commit --sign-tag --sign-archive --add-user --retire-user --audit 'Justus Winter <justus@sequoia-pgp.org>' D2F2C5D45BE9FDE6A4EE0AAF31855247603831FD
  - User "Justus Winter <justus@sequoia-pgp.org>" was granted the right sign-commit.
  - User "Justus Winter <justus@sequoia-pgp.org>" was granted the right sign-tag.
  - User "Justus Winter <justus@sequoia-pgp.org>" was granted the right sign-archive.
  - User "Justus Winter <justus@sequoia-pgp.org>" was granted the right add-user.
  - User "Justus Winter <justus@sequoia-pgp.org>" was granted the right retire-user.
  - User "Justus Winter <justus@sequoia-pgp.org>" was granted the right audit.
```

`sq-git` reads the certificates from the user's certificate store.
Use `sq import < FILE` to import certificates in a file, `sq keyserver
get FINGERPRINT` to fetch certificates from a key server, etc.

The policy file can be viewed as follows:

```shell
$ sq-git policy describe
# OpenPGP policy file for git, version 0

## Commit Goodlist


## Authorizations

0. Justus Winter <justus@sequoia-pgp.org>
   - may sign commits
   - may sign tags
   - may sign archives
   - may add users
   - may retire users
   - may goodlist commits
   - has OpenPGP cert: D2F2C5D45BE9FDE6A4EE0AAF31855247603831FD
1. Neal H. Walfield <neal@pep.foundation>
   - may sign commits
   - may sign tags
   - may sign archives
   - may add users
   - may retire users
   - may goodlist commits
   - has OpenPGP cert: F7173B3C7C685CD9ECC4191B74E445BA0E15C957
```

If you are happy, you can add the policy file (`openpgp-policy.toml`)
to your `git` repository in the usual manner.  But don't forget to tell
`git` to sign commits by adding something like the following to your
repository's `.git/config` file:

```text
[user]
        signingkey = F7173B3C7C685CD9ECC4191B74E445BA0E15C957
        email = 'neal@pep.foundation'
        name = 'Neal H. Walfield'
[commit]
        gpgsign = true
```

Then run:

```shell
$ git add openpgp-policy.toml
$ git commit -m 'Add a commit policy.'
[main (root-commit) 68020e3] Add a commit policy.
 1 file changed, 124 insertions(+)
 create mode 100644 openpgp-policy.toml
```

Now, create a new commit, and verify the new version:

```shell
$ echo 'hello world!' > greeting
$ git add greeting
$ git commit -m 'Say hello.'
$ sq-git log --trust-root 68020e32b6b4ae7ac93de0c215090ba76a410dad
1c9dc0eb3d7692153c2cfc089bc7aad5b4b0df4d: Neal H. Walfield <neal@pep.foundation> [74E445BA0E15C957]
```

Instead of entering the trust root manually, which is error prone, you
can use the special `openpgp-trust-root` branch:

```shell
$ git branch --no-track openpgp-trust-root 68020e32b6b4ae7ac93de0c215090ba76a410dad
$ sq-git log
1c9dc0eb3d7692153c2cfc089bc7aad5b4b0df4d: Cached positive verification
```

## Rejecting Unauthorized Commits

Insert the following line into `hooks/update` on a shared `git` server
to make it enforce the policy embedded in the repository starting at
the given trust root (`<COMMIT>`), which is specified as a hash:

```text
sq-git update-hook --trust-root=<COMMIT> "$@"
```
