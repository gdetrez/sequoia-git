use std::fs;
mod common;
use common::Environment;

fn create_environment() -> anyhow::Result<(Environment, String)> {
    Environment::scooby_gang_bootstrap()
}

#[test]
fn sign_commit() -> anyhow::Result<()> {
    let (e, root) = create_environment()?;
    let p = e.git_state();

    // Bookmark.
    e.git(&["checkout", "-b", "test-base"])?;

    // Willow's code-signing key can change the source code, as she
    // has the sign-commit right.
    e.git(&["checkout", "-b", "test-willow"])?;
    fs::write(p.join("a"), "Aller Anfang ist schwer.")?;
    e.git(&["add", "a"])?;
    e.git(&[
        "commit",
        "-m", "First change.",
        &format!("-S{}", e.willow.fingerprint),
    ])?;

    e.sq_git(&["log", "--trust-root", &root])?;

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // Her release key also has that right, because she needs it in
    // order to give it to new users.
    e.git(&["checkout", "-b", "test-willow-release"])?;
    fs::write(p.join("a"), "Aller Anfang ist schwer.  -- Schiller")?;
    e.git(&["add", "a"])?;
    e.git(&[
        "commit",
        "-m", "Someone is not quite correct on the internet.",
        &format!("-S{}", e.willow_release.fingerprint),
    ])?;

    e.sq_git(&["log", "--trust-root", &root])?;

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // Buffy's cert was not yet added, so she may not sign commits.
    e.git(&["checkout", "-b", "test-buffy"])?;
    fs::write(p.join("a"),
              "Aller Anfang ist schwer, unless you are super strong!1")?;
    e.git(&["add", "a"])?;
    e.git(&[
        "commit",
        "-m", "Well, actually...",
        &format!("-S{}", e.buffy.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());

    Ok(())
}

#[test]
fn add_user() -> anyhow::Result<()> {
    let (e, root) = create_environment()?;

    // Bookmark.
    e.git(&["checkout", "-b", "test-base"])?;

    // Willow's code-signing key can change the source code, but she
    // can not add users.
    e.git(&["checkout", "-b", "test-willow"])?;

    // Try to add Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--sign-commit"
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.willow.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // However, her release key does have that right.
    e.git(&["checkout", "-b", "test-willow-release"])?;

    // Try to add Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--sign-commit"
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.willow_release.fingerprint),
    ])?;

    e.sq_git(&["log", "--trust-root", &root])?;

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // Xander's cert was not yet added, so he definitely may not add
    // users either.
    e.git(&["checkout", "-b", "test-xander"])?;

    // Try to add Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--sign-commit"
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.xander.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());

    Ok(())
}

#[test]
fn retire_user() -> anyhow::Result<()> {
    let (e, root) = create_environment()?;

    // Add Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--sign-commit"
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.willow_release.fingerprint),
    ])?;
    e.sq_git(&["log", "--trust-root", &root])?;

    // Bookmark.
    e.git(&["checkout", "-b", "test-base"])?;

    // Willow's code signing key may not retire Buffy.
    e.git(&["checkout", "-b", "test-willow"])?;

    // Try to retire Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--no-sign-commit",
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.willow.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // However, her release key does have that right.
    e.git(&["checkout", "-b", "test-willow-release"])?;

    // Try to retire Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--no-sign-commit",
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.willow_release.fingerprint),
    ])?;

    e.sq_git(&["log", "--trust-root", &root])?;

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // Xander's cert was not yet added, so he definitely may not
    // retire users either.
    e.git(&["checkout", "-b", "test-xander"])?;

    // Try to retire Buffy.
    e.sq_git(&[
        "policy",
        "authorize",
        e.buffy.petname,
        &e.buffy.fingerprint.to_string(),
        "--no-sign-commit",
    ])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Add Buffy.",
        &format!("-S{}", e.xander.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());

    Ok(())
}

#[test]
fn audit() -> anyhow::Result<()> {
    let (e, root) = create_environment()?;
    let p = e.git_state();

    // Add a commit signed by Xander, who is not authorized to do
    // that.
    fs::write(p.join("a"),
              "Aller Anfang ist schwer, I'll go fetch the hammer!")?;
    e.git(&["add", "a"])?;
    e.git(&[
        "commit",
        "-m", "No problem, we'll get you up and running in no time.",
        &format!("-S{}", e.xander.fingerprint),
    ])?;
    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());
    let bad_commit = e.git_current_commit()?;

    // Bookmark.
    e.git(&["checkout", "-b", "test-base"])?;

    // Now we try to recover by good-listing the bad commit.

    // Willow's code signing key may not goodlist commits.
    e.git(&["checkout", "-b", "test-willow"])?;

    // Try to goodlist the commit.
    e.sq_git(&["policy", "goodlist", &bad_commit])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Goodlist the bad commit.",
        &format!("-S{}", e.willow.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());
    let new_root = e.git_current_commit()?;
    // XXX: Currently, this fails on the assumption that an empty
    // commit range is not valid, b/c that is a vacuous truth.
    assert!(e.sq_git(&["log", "--trust-root", &new_root]).is_err());
    // But, if we look further back, the commit is now goodlisted, but
    // the signer that got the commit goodlisted does not have the
    // audit right.
    assert!(e.sq_git(&["log", "--trust-root", &new_root,
                       &format!("{}..", root)]).is_err());

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // Willow's release key may goodlist commits.
    e.git(&["checkout", "-b", "test-willow-release"])?;

    // Try to goodlist the commit.
    e.sq_git(&["policy", "goodlist", &bad_commit])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Goodlist the bad commit.",
        &format!("-S{}", e.willow_release.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());
    let new_root = e.git_current_commit()?;
    // XXX: Currently, this fails on the assumption that an empty
    // commit range is not valid, b/c that is a vacuous truth.
    assert!(e.sq_git(&["log", "--trust-root", &new_root]).is_err());
    // But, if we look further back, the commit is now goodlisted.
    e.sq_git(&["log", "--trust-root", &new_root,
               &format!("{}..", root)])?;

    // Reset.
    e.git(&["checkout", "test-base"])?;
    e.git(&["clean", "-fdx"])?;

    // Xander's cert was not yet added, so he definitely may not
    // goodlist his own commit.
    e.git(&["checkout", "-b", "test-xander"])?;

    // Try to goodlist the commit.
    e.sq_git(&["policy", "goodlist", &bad_commit])?;
    e.git(&["add", "openpgp-policy.toml"])?;
    e.git(&[
        "commit",
        "-m", "Goodlist the bad commit.",
        &format!("-S{}", e.xander.fingerprint),
    ])?;

    assert!(e.sq_git(&["log", "--trust-root", &root]).is_err());
    let new_root = e.git_current_commit()?;
    // XXX: Currently, this fails on the assumption that an empty
    // commit range is not valid, b/c that is a vacuous truth.
    assert!(e.sq_git(&["log", "--trust-root", &new_root]).is_err());
    // But, if we look further back, the commit is now goodlisted, but
    // the signer that got the commit goodlisted does not have the
    // audit right.
    assert!(e.sq_git(&["log", "--trust-root", &new_root,
                       &format!("{}..", root)]).is_err());

    Ok(())
}
